# Dice Roller

This is a 3D Dice Roller that can be used to throw up to 5 dices using the accelerometer of a phone.

## Usage / Installation

**Warning:** the Browser is only able to read the sensor data if the website is provided using **HTTPS**.

This can be done e.g. using the [VS Code LiveServer](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) extension and enabling HTTPS in there. The path for the keys has to be absolute, so it has to be adjusted in ```.vscode/settings.json```