$(document).ready(async function () {
  $("#roll").click(() => {
    $(".dices .cube").each((index, elem) => {
      const cube = $(elem);
      cube.removeClass("animate")

      let xAmount = 0
      let yAmount = 0

      do {
        xAmount = Math.floor(Math.random() * 32) / 4
      } while (xAmount === 0 || xAmount < 4)

      do {
        yAmount = Math.floor(Math.random() * 32) / 4
      } while (yAmount === 0 || yAmount < 4)

      let xDirection = 0
      let yDirection = 0

      do {
        xDirection = Math.round(Math.random() * 2 - 1)
      } while (xDirection === 0)

      do {
        yDirection = Math.round(Math.random() * 2 - 1)
      } while (yDirection === 0)

      const xTurn = xDirection * xAmount
      const yTurn = yDirection * yAmount

      elem.style.setProperty('--turn-amount-x', `${xTurn}turn`)
      elem.style.setProperty('--turn-amount-y', `${yTurn}turn`)

      cube.addClass("animate")
    })
  })

  $('#dice_amount').on('change', function () {
    const amount = parseInt(this.value)
    const dices = $(".dices")
    let cubeSize = 1;

    switch (amount) {
      case 1:
        cubeSize = 4
        break
      case 2:
        cubeSize = 3
        break
      case 3:
        cubeSize = 2.5
        break
      case 4:
        cubeSize = 2.5
        break
      case 5:
        cubeSize = 2
        break
    }
    document.documentElement.style.setProperty('--cube-size', `${cubeSize}rem`);

    dices.children(".dice").remove()

    for (let dice = 1; dice <= amount; dice++) {

      const diceElement = $(".dice_template").clone()
      diceElement.removeClass("dice_template")
      diceElement.addClass("dice")
      diceElement.show()

      dices.append(diceElement)
    }
  })

  $('#dice_amount').change()


  const hasDeviceMotion = 'ondevicemotion' in window;
  if (hasDeviceMotion) {
    window.addEventListener('devicemotion', onMotion, true);
  }

  const threshold = 15;
  const timeout = 1000;

  let lastTime = new Date();

  let lastX = null;
  let lastY = null;
  let lastZ = null;

  function onMotion(e) {
    const currentRotation = e.accelerationIncludingGravity;

    let currentTime;
    let timeDifference;

    let deltaX = 0;
    let deltaY = 0;
    let deltaZ = 0;
  
    if ((lastX === null) && (lastY === null) && (lastZ === null)) {
      lastX = currentRotation.x;
      lastY = currentRotation.y;
      lastZ = currentRotation.z;
      return;
    }
  
    deltaX = Math.abs(lastX - currentRotation.x);
    deltaY = Math.abs(lastY - currentRotation.y);
    deltaZ = Math.abs(lastZ - currentRotation.z);
  
    if (((deltaX > threshold) && (deltaY > threshold)) 
    || ((deltaX > threshold) && (deltaZ > threshold)) 
    || ((deltaY > threshold) && (deltaZ > threshold))) {
      currentTime = new Date();
      timeDifference = currentTime.getTime() - lastTime.getTime();
  
      if (timeDifference > timeout) {
        $("#roll").click()
        lastTime = new Date();
      }
    }
  }

  if (window.location.protocol !== "https:") {
    $("#shake").hide()
    $("#shake_not_possible").show()
  }
})
